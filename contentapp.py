#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        metodo,recurso,cuerpo = parsedRequest

        if metodo == "POST":
            self.content[recurso] = cuerpo.split("=")[1]
            httpCode = "200 OK"
            htmlBody = "<html>" \
                           "<body>" \
                               "<p>Nuevo valor: " + recurso + "</p>" \
                                "<p>Valor Nuevo: " + self.content[recurso] + "</p>" \
                                "<p>Introduce algo si lo quieres cambiar: </p>" + \
                                "<html>" \
                                    "<body>" \
                                        "<form action = ' ' method = 'POST' >" \
                                            "<p>Valor: <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                        "</form>" \
                                    "</body>" \
                                "</html>"

            return (httpCode,htmlBody)

        if recurso in self.content.keys():
            httpCode = "200 OK"
            htmlBody = "<html>" \
                            "<body>" + recurso + \
                                " encontrado.</p><p>Valor: " + self.content[recurso] + "</p>" \
                                "<p>Si quiere actualizar el valor: </p>" + \
                                "<html>" \
                                    "<body>" \
                                        "<form action = ' ' method = 'POST' >" \
                                            "<p>Valor: <input type = 'text' name = 'nombre' value = ''/>" + "</p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                        "</form>" \
                                    "</body>" \
                                "</html>"

        else:
            httpCode = "404 Not Found"
            htmlBody = "<html>" \
                           "<body>" \
                                "<p>Recurso: " + recurso + " no encontrado, dale un valor: </p><br><br>" + \
                                "<html>" \
                                    "<body>" \
                                        "<form action = ' ' method = 'POST' >" \
                                            "<p>Valor: <input type = 'text' name = 'nombre' value = ''/>" + "</p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" + \
                                        "</form>" \
                                    "</body>" \
                                "</html>"

        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)

